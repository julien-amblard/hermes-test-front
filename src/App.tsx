import Stack from "react-bootstrap/Stack";
import Container from "react-bootstrap/Container";

function App() {
  return (
    <Stack gap={3}>
      <Container
        className="d-flex align-items-center justify-content-center bg-secondary-subtle"
        fluid
        style={{ height: "300px" }}
      >
        <h1>Hermes Digital</h1>
      </Container>

      <Container>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident
        soluta quae, minima eveniet natus commodi doloribus, minus ullam quaerat
        in voluptate recusandae impedit, eligendi perspiciatis explicabo quam
        assumenda rem? Sequi.
      </Container>
    </Stack>
  );
}

export { App };
